# Dotfiles used on my i3 system

### vim configs
- Add custom snippets

### zsh
- Install zsh: `sudo apt install zsh`
- Install oh-my-zsh: <https://github.com/robbyrussell/oh-my-zsh>
- Install k plugin: <https://github.com/supercrabtree/k>
- Set zsh as default shell:  `chsh -s $(which zsh)`

### URxvt
- Set URxvt as default: `sudo update-alternatives --config x-terminal-emulator`
